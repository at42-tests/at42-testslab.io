# at42-tests.gitlab.io
This repository is where we host the source code of
[https://at42-tests.gitlab.io](https://at42-tests.gitlab.io).

We use it so our test scripts are easily accessible.

## Usage
```sh
bash <(curl -sSL at42-tests.gitlab.io)
```
